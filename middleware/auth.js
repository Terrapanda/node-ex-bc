const bcrypt = require('bcrypt');
const { ExtractJwt, Strategy } = require('passport-jwt');

const CONFIG = require('../config/config');
const getCatQuery = require('../db_apis/cats.js');
const { to, TE } = require('../services/util.js');


const customTokenExtractor = function(req) {
  let token = null;
    if (req && req.header('authorization'))
      token = req.header('authorization');
    return token;
};

module.exports.JWTPassportFunc = function(passport, req){
    const opts = {};

    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = CONFIG.jwt_encryption;

    passport.use(new Strategy(opts, async function(jwt_payload, done) {

        let [err, user] = await to(getCatQuery.find({ id: jwt_payload.user_id }));

        if(err) return done(err, false);
        if(user) return done(null, user);

        return done(null, false);
    }));
}

module.exports.hashPassFunc = async function(req, res, next) {
  let hash;

  let [err, salt] = await to(bcrypt.genSalt(10));
  if (err)
    TE(err.message, true);

  [err, hash] = await to(bcrypt.hash(String(req.body.password), salt));
  if (err)
    TE(err.message, true);

  req.body.preHashPass = String(req.body.password);
  req.body.password = hash;

  next();
}
