const CONFIG = require('../config/config');

module.exports = {
  catPool: {
    connectionLimit: CONFIG.KITTY_CONNECTION_LIMIT,
    database: CONFIG.KITTY_DATABASE,
    host: CONFIG.KITTY_CONNECTIONSTRING,
    password: CONFIG.KITTY_PASSWORD,
    user: CONFIG.KITTY_USER,
    port: CONFIG.KITTY_DATABASE_PORT
  }
};
