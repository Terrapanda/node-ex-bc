CONFIG = {};

CONFIG.jwt_algorithm=process.env.JWT_ALGORITHM || 'HS256';
CONFIG.jwt_encryption=process.env.JWT_ENCRYPTION || 'needmorecatlovers';
CONFIG.jwt_expiration=process.env.JWT_EXPIRATION || '10000';

CONFIG.HTTP_PORT=process.env.HTTP_PORT || 8081;

CONFIG.KITTY_USER=process.env.RDS_USERNAME || "kitty";
CONFIG.KITTY_PASSWORD=process.env.RDS_PASSWORD || "bluecrew";
CONFIG.KITTY_CONNECTIONSTRING=process.env.RDS_HOSTNAME || "localhost";
CONFIG.KITTY_DATABASE=process.env.RDS_DB_NAME || "bluecrew_cats";
CONFIG.KITTY_DATABASE_PORT=process.env.RDS_PORT || 3306;
CONFIG.KITTY_CONNECTION_LIMIT=10;


CONFIG.invalid_login_credential = "The username and password combination you entered is incorrect. Please try again.";

module.exports = CONFIG;
