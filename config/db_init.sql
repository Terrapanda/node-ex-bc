CREATE DATABASE bluecrew_kitty;
CREATE TABLE IF NOT EXISTS `cats` (
  `addedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `birthdate` date,
  `breed` varchar(100),
  `lastSeenAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastUpdated` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `imageURL` varchar(255),
  `name` varchar(100) NOT NULL,
  `password` varchar(60) NOT NULL,
  `username` varchar(32) NOT NULL,
  `weight` float(5,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

ALTER TABLE bluecrew_cats CHANGE `user_name` `username` varchar(16);


INSERT INTO cat (id, name,password,user_name,weight) VALUES (1, 'test_name','test','test',3.2)
INSERT INTO cat (id, name,password,user_name,weight) VALUES (1, 'test_name','test','test',3.2)
INSERT INTO cat (id, name,password,user_name,weight) VALUES (1, 'test_name','test','test',3.2)


{
    "birthdate": "2018-01-01",
    "breed": "Jungle Cat",
    "imageURL": "http://www.catsg.org/typo3temp/pics/89861ce7f9.jpg",
    "name": "Odin",
    "password": "nine9lives",
    "user_name": "Codin-Odin",
    "weight": 11.02
}

SELECT lastseen FROM users WHERE Id = $desired_user_id


$lastSeen= mysqli_query($con, "UPDATE ws_users SET us_lastseen=NOW() WHERE us_id=$user_id");


-- do I want this pagination ?
--
-- if (context.skip) {
--   binds.row_offset = context.skip;
--
--   query += '\noffset ? rows';
-- }
--
-- const limit = (context.limit > 0) ? context.limit : 30;
--
-- binds.row_limit = limit;
--
-- query += '\nfetch next ? rows only';


-- In SQL, the order by clause is used to sort data. Unfortunately, it is not possible to bind in the column name in the order by clause of a SQL query as it’s considered an identifier rather than a value. This means you’ll need to be very careful when appending the column name and direction to the query to prevent SQL injection. You could sanitize the values passed in or compare them against a whitelist of values. I’ll use the whitelist approach as it provides more control than generic sanitization.


if (context.id) {
  binds.id = context.id;

  query += '\nand id = ?';
}

if (context.name) {
  binds.name = context.name;

  query += '\nand name = ?';
}

if (context.user_name) {
  binds.user_name = context.user_name;

  query += '\nand user_name = ?';
}
