const express = require('express');
const passport = require('passport');

const catsController = require('../controllers/cats.js');
const { hashPassFunc, JWTPassportFunc } = require('./../middleware/auth.js');

const router = new express.Router();

router.use('/cats?', function(req, res, next) {
  JWTPassportFunc(passport, req);
  next();
});

router.route('/cats/')
  .get(passport.authenticate('jwt', {session:false}), catsController.get);

router.route('/cats/random/')
  .get(passport.authenticate('jwt', {session:false}), catsController.getRandom);

router.route('/cat/login/')
  .post(catsController.login);

router.route('/cat/register/:id?')
  .delete(passport.authenticate('jwt', {session:false}), catsController.delete)
  .post(hashPassFunc, catsController.register)
  .put(passport.authenticate('jwt', {session:false}), catsController.update);



module.exports = router;
