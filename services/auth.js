const bcrypt_p = require('bcrypt-promise');
const jwt = require('jsonwebtoken');

const CONFIG = require('../config/config');
const getCatQuery = require('../db_apis/cats.js');
const { to, TE } = require('../services/util.js');

async function getJWT(body) {

  let expiration_time = parseInt(CONFIG.jwt_expiration);

  const token = "Bearer "+jwt.sign(
    {user_id: body.id},
    CONFIG.jwt_encryption,
    {expiresIn: expiration_time}
  );

  return token;
};

async function getComparePassword(body, record, next) {
  if(!record.password) next('password not set');

  let [err, pass] = await to(bcrypt_p.compare(String(body.password), record.password));
  if(err) TE(err);
  debugger;
  if(!pass)
    next(CONFIG.invalid_login_credential);

  return record;
}

async function authUser(body, next) {

  let [err, record] = await to(getCatQuery.find({ limit: 1, search: { username: String(body.username) }}));
  if(err) TE(err);

  if(!record)
    next(CONFIG.invalid_login_credential);

  [err, record] = await to(getComparePassword(body, record, next));
  if(err) next(err);

  const token = await getJWT(record)

  return { token: token, user: record };
}

module.exports.authUser = authUser;
module.exports.getComparePassword = getComparePassword;
module.exports.getJWT = getJWT;
