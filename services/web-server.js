
const bodyParser = require('body-parser')
const express = require('express');
const http = require('http');
const morgan = require('morgan');
const passport = require('passport');
const cors = require('cors');
const { to, ReE }  = require('../services/util.js');
const pe            = require('parse-error');''

const router = require('./router.js');
const webServerConfig = require('../config/web-server.js');

let httpServer;

function initialize() {
  return new Promise((resolve, reject) => {
    const app = express();
    httpServer = http.createServer(app);

    app.use(bodyParser.urlencoded({
      extended: true
    }));

    app.use(bodyParser.json())
    app.use(morgan('combined'));
    app.use(passport.initialize());
    app.use(cors());
    app.use('/api', router);

    // error handler
    app.use(function(err, req, res, next) {
      // set locals, only providing error in development
      res.locals.message = err.message;
      res.locals.error = req.app.get('env') === 'development' ? err : {};

      // handle  uncaught errors
      res.status(err.status || 500);
      return ReE(res, err, err.status);

    });

    // This is here to handle uncaught promise rejections
    process.on('unhandledRejection', error => {
        console.error('Uncaught Error', pe(error));
        return;
    });


    httpServer.listen(webServerConfig.port)
      .on('listening', () => {
        console.log(`Web server listening on localhost:${webServerConfig.port}`);

        resolve();
      })
      .on('error', err => {
        reject(err);
      });
  });
}

function close() {
  return new Promise((resolve, reject) => {
    httpServer.close((err) => {
      if (err) {
        reject(err);
        return;
      }

      resolve();
    });
  });
}

module.exports.close = close;
module.exports.initialize = initialize;
