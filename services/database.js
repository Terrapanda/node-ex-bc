const mysql = require('mysql');
const { promisify } = require('util');

const dbConfig = require('../config/database.js');

const pool = mysql.createPool(dbConfig.catPool);

async function initialize() {
  pool.getConnection((err, connection) => {
      if (err) {
          if (err.code === 'PROTOCOL_CONNECTION_LOST') {
              console.log('Database connection was closed.')
          }
          if (err.code === 'ER_CON_COUNT_ERROR') {
              console.log('Database has too many connections.')
          }
          if (err.code === 'ECONNREFUSED') {
              console.log('Database connection was refused.')
          }
      }
      if (connection) connection.release()
      return
  })
}

async function close() {
  await pool.end();
}

pool.query = promisify(pool.query)

module.exports.pool = pool
module.exports.initialize = initialize
