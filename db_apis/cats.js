const CONFIG = require('../config/config');

const { pool } = require('../services/database.js');
const { TE, to } = require('../services/util.js');


const baseQuery =
 `select id "id",
    birthdate "birthdate",
    breed "breed",
    imageURL "imageURL",
    name "name",
    password "password",
    username "username",
    weight "weight"
  from cats
  where 1 = 1`;

const sortableColumns = [
  'birthdate',
  'breed',
  'id',
  'lastSeenAt',
  'name',
  'username',
  'weight',
];

async function findUserByUsername(username) {
  let [err, record] = await to(find({ limit: 1, search: { username: username }}));
  if (!record)
    return null;

  return record;
}

async function findRandom(context) {

  let [err, result] = await to (pool.query(`select * from cats order by rand() limit 1;`));
  if(err)
    TE(err)

  return result;
}


async function find(context) {
  let query = baseQuery;
  const bindings = [];

  // search by id, name, and username
  if (!!context.search) {

    if (!!context.search.id) {
      bindings.push(context.search.id);

      query += '\nand id = ?';
    }

    if (!!context.search.name) {
      bindings.push(context.search.name);

      query += '\nand name = ?';
    }

    if (!!context.search.username) {
      bindings.push(context.search.username);

      query += '\nand username = ?';
    }
  }

  // sort custom and default by whitelisted fields
  if (context.sort === undefined) {

    query += '\norder by lastSeenAt desc';

  } else {

    let [column, order] = context.sort.split(':');

    if (!sortableColumns.includes(column))
      TE('Invalid "sort" column');

    if (order === undefined)
      order = 'desc';

    if (order !== 'asc' && order !== 'desc')
      TE('Invalid "sort" order');

    query += `\norder by ${column} ${order}`;
  }

  // pagination limits search results
  const limit = (context.limit > 0) ? context.limit : 100;

  bindings.push(limit);

  query += '\n limit ?';

  const result = await pool.query(query, bindings);

  return context.limit === 1 ? result[0] : result;
}

const registerSql =
 `insert into cats set ?`;

async function register(user) {

  let [err, result] = await to (pool.query(registerSql, user));
  if(err)
    TE(err)

   if (result.affectedRows && result.affectedRows === 1)
     user.id = result.insertId

  return user;
}

const updateSql =
 `update cats set ? where id = ?`;

async function update(user, id) {

  let [err, result] = await to (pool.query(updateSql, [user, id]));
  if (err)
    TE(err)

  if (result && result.affectedRows === 1)
    return user;

  return null;
}

const deleteSql =
 `delete from cats where id = ?;`

async function del(id) {

  let [err, result] = await to (pool.query(deleteSql, [id]));
  if(err)
    TE(err);

  if (result && result.affectedRows === 1)
    return result;

  return null;
}

const lastSeenSql =
`update cats set lastSeenAt=now() where id = ?;`

async function lastSeen(id) {;

  let [err, result] = await to (pool.query(lastSeenSql, [id]));
  if(err)
    TE(err)

  if (result.affectedRows && result.affectedRows === 1)
    return result;

  return null;
}


module.exports.register = register;
module.exports.delete = del;
module.exports.find = find;
module.exports.findRandom = findRandom;
module.exports.findUserByUsername = findUserByUsername;
module.exports.lastSeen = lastSeen;
module.exports.update = update;
